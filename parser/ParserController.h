
// ParserController header
//
// Parser is controlled from here
//
// Written by X.Hunt

#ifndef PARSERCTRL_H
#define PARSERCTRL_H

#include <string>
#include <unordered_map>

#include "DefinableObject.h"
#include "Parser.h"
#include "SQLWrapper.h"

class ParserController {

public:

	// Database things
	void setDatabaseName(std::string dbName);
	std::string getDatabaseName();
	int createDB();

	// Actually parse things
	int parseDirectory(std::string dirName);
	int parse(std::string file);
	
private:

	Parser parser;
	std::string databaseName;
	std::unordered_map<std::string, DefinableObject> objects;
	SQLWrapper sqlWrapper;	// May not need?

	// Flags
	bool dbReady = false;
	
};

#endif