
// X library
//
// Written by X.Hunt
//
// This library contains helper functions for all the things!

#include "X.h"
#include <functional>

// =========================
// String

void X::split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}
}


std::vector<std::string> X::split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

// http://stackoverflow.com/a/24315631
//
// std::string s = X::replaceAll(string("Number Of Beans"), std::string(" "), std::string("_")) => Number_Of_Beans
std::string X::replaceAll(std::string str, const std::string& from, const std::string& to) {
	size_t start_pos = 0;
	while((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length();
	}
	return str;
}

// Small little magic function to convert a string to a number.
// The number MUST be different for every unique string
int X::stringToNumber(const std::string &str) {
	std::hash<std::string> hash;
	return ((int)hash(str) * (int)hash(str));		// Converts to a positive number for ease of reading. May not always be > 0, but should mostly be
}

// http://stackoverflow.com/a/16575025
bool X::isNumber(const std::string &str) {
	char* p;
	strtol(str.c_str(), &p, 10);
	return *p == 0;
}

// =========================
// Vector

// Split the line by strings
std::vector<std::string> X::splitStringBySpace(std::string line) {
	std::string buf;
	std::stringstream ss(line);

	std::vector<std::string> tokens;

	while (ss >> buf)
		tokens.push_back(buf);

	return tokens;

}

// =========================
// Stack

void X::printStack(std::stack<std::string>* stack) {


	std::cout << "=================" << std::endl;
	std::cout << "Stack contains the following:" << std::endl;

	while(!stack->empty()) {
		std::cout << stack->top() << std::endl;
		stack->pop();
	}

	std::cout << "=================" << std::endl;
}

// =========================
