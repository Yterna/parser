
// Main external API
//
// All external things should only need to interact with this
//
// Written by X.Hunt

#ifndef PARSER_API
#define PARSER_API

// Includes
#include "ParserController.h"

#include <vector>
#include <functional>

class ParserAPI {

private:
	
	// Internal parser controller
	ParserController _parserController;
	

public:

	static ParserAPI INSTANCE;
	static ParserAPI* getInstance();

	// Database things
	void setDatabaseName(std::string dbName);
	std::string getDatabaseName();
	void createDatabase();
	void createAndNameDatabase(std::string dbName);

	// Parser things
	int parseDirectory(std::string directory);
	int parseFile(std::string file);

	// Object things
	DefinableObject getObjectByName(std::string objectClassName);
	void storeObject(DefinableObject object);
	void displayObject(DefinableObject object);
	void displayObject(std::string objectClassName);

	std::vector<DefinableObject> getAllObjects();

	// Callbacks
	void registerCallback(std::string regexString, std::function<std::string(std::string)> callbackFunc);
	std::string runCallback(std::string token);


};

#endif