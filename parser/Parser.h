
// Parser header
//
// The parser parses YLD files
//
// Written by X.Hunt

#ifndef PARSER_H
#define PARSER_H

// Includes
#include <vector>
#include <string>

#include "DefinableObject.h"

// ===================

class Parser {

public:

	int parse(std::vector<DefinableObject>* objects);

	void setSingleFile(std::string file);
	std::string getSingleFile();

	void setRootDirectory(std::string dir);
	std::string getRootDirectory();

private:
	std::string rootDirectory;
	std::string singleFile;
	int ID = 0;

};

#endif