
#include "SQLWrapper.h"

#include "Property.h"
#include <fstream>
#include "lib/sqlite3.h"
#include "X.h"

/* Static callbacks for returning things out of the DB */

// Generic callback
static int callback(void *data, int argc, char **argv, char **azColName){
	std::cout << "----------" << std::endl;
	for(int i = 0; i < argc; i++) {
		std::cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
	}
	return 0;
}

// Grabs a definable object
static int defObjCallback(void *data, int argc, char **argv, char **azColName){
	
	DefinableObject defObj;

	defObj.setClassName(argv[1]);
	//defObj.setProperty("Type", argv[2]);

	((std::vector<DefinableObject>*)(data))->push_back(defObj);
	return 0;
}

// Grabs a property
static int propertyCallback(void *data, int argc, char **argv, char **azColName) {

	std::string key = X::split(argv[0], ':')[1];
	std::string value = argv[1];

	Property prop(key, value);

	((std::vector<Property>*)(data))->push_back(prop);

	return 0;
}

// Grabs an int
static int intCallback(void *count, int argc, char **argv, char **azColName) {
	int* c = (int *)count;
	*c = atoi(argv[0]);
	return 0;
}

/* SQL */

// Runs just an SQL query
int SQLWrapper::runSQL(std::string databaseName, std::string sql) {

	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;

	//std::cout << "SQL: Running `" << sql  <<  "`" << std::endl;

	rc = sqlite3_open(databaseName.c_str(), &db);

	if(rc) {
		std::cerr << "SQL: Error: Can't open database " << databaseName << ": "  << sqlite3_errmsg(db) << std::endl;
		return EXIT_FAILURE;
	}

	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK) {
		std::cerr << "SQL: Error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
	}

	sqlite3_close(db);

	return EXIT_SUCCESS;

}

// Runs an SQL query, and takes in a data blob
int SQLWrapper::runSQL(std::string databaseName, std::string sql, void* data) {

	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;

	//std::cout << "SQL: Running `" << sql  <<  "`" << std::endl;

	rc = sqlite3_open(databaseName.c_str(), &db);

	if(rc) {
		std::cerr << "SQL: Error: Can't open database " << databaseName << ": "  << sqlite3_errmsg(db) << std::endl;
		return EXIT_FAILURE;
	}

	rc = sqlite3_exec(db, sql.c_str(), callback, data, &zErrMsg);

	if (rc != SQLITE_OK) {
		std::cerr << "SQL: Error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
	}

	sqlite3_close(db);

	return EXIT_SUCCESS;

}

// Runs an SQL query, with a data blob and a callback function
int SQLWrapper::runSQL(std::string databaseName, std::string sql, void* data, int (*callback)(void*,int,char**,char**)) {

	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;

	//std::cout << "SQL: Running `" << sql  <<  "`" << std::endl;

	rc = sqlite3_open(databaseName.c_str(), &db);

	if(rc) {
		std::cerr << "SQL: Error: Can't open database " << databaseName << ": "  << sqlite3_errmsg(db) << std::endl;
		return EXIT_FAILURE;
	}

	rc = sqlite3_exec(db, sql.c_str(), callback, data, &zErrMsg);

	if (rc != SQLITE_OK) {
		std::cerr << "SQL: Error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
	}

	sqlite3_close(db);

	return EXIT_SUCCESS;

}

/* DefObj */

int SQLWrapper::storeDefObj(std::string databaseName, DefinableObject* defObj) {

	std::unordered_map<std::string, Property>* properties = defObj->getProperties();

	std::string parentStr = "null";
	std::string name = "null";
	std::string type = "none";
	int ID;

	std::unordered_map<std::string, Property>::iterator prop;

	// The type is text before the first dot. Where there's no subclass, the type is the same as the classname
	type = defObj->getClassName().substr(0, defObj->getClassName().find("."));

	if(defObj->hasProperty("__internalClassName")) {
		name = defObj->getProperty("__internalClassName").getValue();
	}

	if(defObj->hasProperty("parent")) {
		parentStr = defObj->getProperty("parent").getValue();
	}

	else {
		parentStr = name;
	}

	// ID
	if(defObj->hasProperty("__internalID")) {
		ID = stoi(defObj->getProperty("__internalID").getValue());
	}
	// If no ID tag, give it the next ID available
	else {
		std::string sql = "select MAX(ID) from DefObjs;";
		runSQL(databaseName, sql, &ID, intCallback);
		defObj->setProperty("__internalID", std::to_string(ID));
	}

	// Object

	std::string sql = "";
	sql += "INSERT OR REPLACE INTO DefObjs (ID, Name, Type)";
	sql += " VALUES(" + std::to_string(ID) + ", '" + name + "', '" + type + "')";

	runSQL(databaseName, sql);

	// Properties

	for (auto p = properties->begin(); p != properties->end(); ++p) {

		Property property = p->second;

		sql = "INSERT OR REPLACE INTO Property (pk_Key, Value, fk_DefObjID)";
		sql += " VALUES(";

		std::string propName = defObj->getClassName() + ":" + property.getKey();
		std::string propValue = property.getValue();

		sql += "'" + propName + "', '" + propValue + "', " + std::to_string(ID) + ")";

		runSQL(databaseName, sql);

	}

	return EXIT_SUCCESS;
}

std::vector<DefinableObject> SQLWrapper::getDefObjs(std::string databaseName) {

	std::vector<DefinableObject> allObjects;
	
	std::string sql;

	// Get the number of items in the DefObjs db
	//int count;
	sql = "SELECT obj.* FROM DefObjs obj";
	SQLWrapper::runSQL(databaseName, sql, &allObjects, defObjCallback);

	// Get the properties for this object
	for(int i = 0; i < allObjects.size(); i++) {

		std::vector<Property> properties;
		sql = "SELECT prop.* FROM Property prop WHERE prop.fk_DefObjID = " + std::to_string(i);
		SQLWrapper::runSQL(databaseName, sql, &properties, propertyCallback);

		for(int j = 0; j < properties.size(); j++) {
			Property prop = properties[j];
			allObjects[i].setProperty(prop.getKey(), prop.getValue());
		}

		allObjects[i].setClassName(allObjects[i].getProperty("__internalClassName").getValue());
	}

	return allObjects;

}

DefinableObject SQLWrapper::getObjectByName(std::string databaseName, std::string objectName) {

	// Iterate through, and look for the objectName given.
	// Return it if found
	std::vector<DefinableObject> allObjects = getDefObjs(databaseName);
	for(int i = 0; i < allObjects.size(); i++) {
		if(allObjects[i].getClassName() == objectName)
			return allObjects[i];
	}

	// If no object with that name can be found, return a blank object so that things dont break down the line
	std::cout << "Parser: Error: No object found with name: " << objectName << std::endl;
	DefinableObject nullObj;
	nullObj.setClassName("NullObject");
	nullObj.setProperty("__internalID", std::to_string(-1));

	return nullObj;

}

/* Database */

int SQLWrapper::createDB(std::string databaseName) {

	std::cout << "Creating database at " << databaseName << std::endl;

	// Tables need to be re-created at runtime (for now!). Dropping them is easiest :)
	runSQL(databaseName, "DROP TABLE DefObjs");
	runSQL(databaseName, "DROP TABLE Property");

	if(createTable(databaseName, "DefObjs") == EXIT_SUCCESS) {
		addColumn(databaseName, "DefObjs", "Name");
		addColumn(databaseName, "DefObjs", "Type");

	}

	if(createTable(databaseName, "Property", "pk_Key") == EXIT_SUCCESS) {
		addColumn(databaseName, "Property", "Value");

		// Add a foreign key to the Property table
		std::string sql = "ALTER TABLE Property ADD COLUMN fk_DefObjID INTEGER REFERENCES defObjs(ID);";
		runSQL(databaseName, sql);
	}

	return EXIT_SUCCESS;
}

// Uses a TEXT for the PK
int SQLWrapper::createTable(std::string databaseName, std::string tableName, std::string PK) {

	std::cout << "--------------" << std::endl;
	std::cout << "Creating table " << tableName << " in database " << databaseName << std::endl;
	std::cout << "--------------" << std::endl;

	std::string sql = "";

	sql += "CREATE TABLE " + tableName + "(";
	sql += PK + " TEXT PRIMARY KEY NOT NULL);";

	return runSQL(databaseName, sql);

}

// Uses an INT for the PK
int SQLWrapper::createTable(std::string databaseName, std::string tableName) {

	std::cout << "--------------" << std::endl;
	std::cout << "Creating table " << tableName << " in database " << databaseName << std::endl;
	std::cout << "--------------" << std::endl;

	std::string sql = "";

	sql += "CREATE TABLE " + tableName + "(";
	sql += "ID INT PRIMARY KEY NOT NULL);";

	return runSQL(databaseName, sql);

}

int SQLWrapper::addColumn(std::string databaseName, std::string tableName, std::string columnName) {

	std::string sql = "";

	sql += "ALTER TABLE " + tableName;
	sql += " ADD " + columnName + " TEXT";

	return runSQL(databaseName, sql);

}








