
// Parser header
//
// A property is a key:value mapping, where:
// 		- The key is a string
//		- The value is a raw string, that will be interpreted at runtime
//
// Each property also has a lambda callback, which can be overridden (but there's no need),
// if the property needs to be interpreted differently
//
// Written by X.Hunt

#ifndef PROPERTY_H
#define PROPERTY_H

// Includes
#include <string>
#include <functional>

class Property {

public:
	Property();
	Property(std::string key, std::string value);
	
	/* Getters */

	std::string getKey();
	std::string getValue();
	std::string getEvaluated();

	/* Setters */

	void updateValue(std::string newVal);
	void setLambda(std::function<std::string(std::string)> lambda);

private:
	std::string _key;
	std::string _value;

	// Default static function that evaluates the property
	static std::string defaultPropertyLambda(std::string s);

	// The function that is used to evaluate the property.
	// This function MUST have the following signature:
	// 		static string funcName(string str)
	std::function<std::string(std::string)> lambdaFunction = defaultPropertyLambda;

	// The result is computed ahead of time (when the lambda or value is updated), and bound to this value
	std::function<std::string()> bindValue;

};

#endif