
// Callback implementation
//
// Written by X.Hunt

#include "CallbackManager.h"

// ==========

CallbackManager CallbackManager::INSTANCE;

void CallbackManager::registerCallback(std::string regexString, std::function<std::string(std::string)> callbackFunc) {

	std::cout << "Registering '" << regexString << "' as a callback" << std::endl;

	_callbacks[regexString] = callbackFunc;
}

std::string CallbackManager::runCallback(std::string token) {

	// Iterate the keys, looking for a match to the token.
	// If found, return the result of the callback, otherwise, just return the original string
	for(auto kv : _callbacks) {
		
		std::string s = kv.first;
		std::function<std::string(std::string)> f = kv.second;

		std::regex r(s);

		if(regex_match(token, r)) {
			return f(token);
		}
	}

	return token;

}