
// SQLWrapper header
//
// This wraps all the nasty SQL calls into something pleasent
//
// Written by X.Hunt

#ifndef SQLWRAPPER_H
#define SQLWRAPPER_H

#include <vector>
#include <string>
#include <iostream>

#include "DefinableObject.h"

class SQLWrapper {

public:
	/* SQL */
	// Various ways to run an SQL queary. Some take a blob of data, and some take a callback function
	static int runSQL(std::string databaseName, std::string sql);
	static int runSQL(std::string databaseName, std::string sql, void* data);
	static int runSQL(std::string databaseName, std::string sql, void* data, int (*callback)(void*,int,char**,char**));

	/* DefObj */
	static int storeDefObj(std::string databaseName, DefinableObject* defObj);
	static std::vector<DefinableObject> getDefObjs(std::string databaseName);
	static DefinableObject getObjectByName(std::string databaseName, std::string objectName);

	/* Database */
	int createDB(std::string databaseName);
	int createTable(std::string databaseName, std::string tableName, std::string PK);
	int createTable(std::string databaseName, std::string tableName);
	int addColumn(std::string databaseName, std::string tableName, std::string column);

};

#endif