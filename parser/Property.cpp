
#include <sstream>
#include <iostream>
#include <vector>
#include <regex>

#include "Property.h"
#include "SQLWrapper.h"
#include "DefinableObject.h"
#include "CallbackManager.h"
#include "ParserAPI.h"

#include "X.h"

// Maths parser
// http://warp.povusers.org/FunctionParser/
#include "lib/fparser.hh"
#include <cstdio>

// Default property lambda.
// This will evaluate the property's value to an actual thing.
// This is THE magic function.

std::string Property::defaultPropertyLambda(std::string s) {

	// Regex patterns
	std::regex REGEX_MATHS(".+ [\\+\\-\\/\\*] .+");
	std::regex REGEX_SYMBOL("[\\+\\-\\/\\*]");
	std::regex REGEX_REFERENCE("^@.*");
	std::regex REGEX_LOGIC("^\\{.+\\}.*");

	std::string callbackCharacter = "~";
	std::regex REGEX_CALLBACK("^" + callbackCharacter + ".+" + callbackCharacter + "$");

	// Initial padding of certain characters
	s = X::replaceAll(s, "{", "{ ");
	s = X::replaceAll(s, "}", " }");

	std::string output;

	// Split the string by spaces, into a set of tokens
	std::vector<std::string> tokens;
	std::string buf;
	std::stringstream ss(s);
	while (ss >> buf)
		tokens.push_back(buf);

	// Convert any reference tokens to an actual value
	for(int i = 0; i < tokens.size(); ++i) {
		std::string tok = tokens[i];

		/* Refernce was found
		 * @Something
		 */

		if(regex_match(tok, REGEX_REFERENCE)) {
			//std::cout << "Lambda: Reference found! " << tok << std::endl;

			// If there is nothing after an initial colon, stop here
			// (This fixes that parent crash, Lewis ;) )
			if(X::split(tok, ':').size() <= 1)
				tokens[i] = tok;

			else {
				// Attempt to get the property referred to
				std::string className = X::split(tok, ':')[0];
				std::string propName = X::split(tok, ':')[1];

				className.erase(std::remove(className.begin(), className.begin() + 1, '@'), className.begin() + 1);

				DefinableObject refObj = SQLWrapper::getObjectByName(ParserAPI::getInstance()->getDatabaseName(), className);
				std::string refValue = refObj.getProperty(propName).getEvaluated();

				tokens[i] = refValue;
			}

		}

		/* Callback was found, eg:
		 * ~1d6~
		 */

		if(regex_match(tok, REGEX_CALLBACK)) {
			tok.erase(std::remove(tok.begin(), tok.end(), callbackCharacter.c_str()[0]), tok.end());

			tokens[i] = CallbackManager::INSTANCE.runCallback(tok);

		}

		if(i != 0)
			output += " ";

		output += tokens[i];
	}

	// Once references and callbacks are sorted, look for certain patterns

	/* Maths was found */
	if(regex_match(output, REGEX_MATHS)) {

		// Set up the function parser library and pass things over to that
		FunctionParser fparser;
		int res = fparser.Parse(output, "x");

		// res is -1 if succeeded, otherwise, it's the position at which the expressin failed
		if(res < 0) {

			double vals[] = { 0 };
			double result = fparser.Eval(vals);

			output = std::to_string(result);

			// Recurse until just a std::string
			return defaultPropertyLambda(output);
		}

		else
			std::cout << "Lambda: Error in expression! '" << output << "' at position: " << res << std::endl;

	}

	/* Logic was found */
	if(regex_match(output, REGEX_LOGIC)) {
		
		// Parse through the line to find:
		//	- the expression
		//	- an optional true value
		// 	- an optional false value

		std::vector<std::string> parts = X::split(output, ':');
		std::string expr = parts[0];

		// Dont need braces any more
		expr.erase(std::remove(expr.begin(), expr.end(), '{'), expr.end());
		expr.erase(std::remove(expr.begin(), expr.end(), '}'), expr.end());

		std::string resultTrue = "true";
		std::string resultFalse = "false";

		// Strip any whitespace from the start and end of the true value (this is so it outputs cleanly)
		if(parts.size() > 1) {
			resultTrue = parts[1];
			resultTrue.erase(std::remove_if(resultTrue.begin(), resultTrue.begin() + 1, ::isspace), resultTrue.begin() + 1);
			resultTrue.erase(std::remove_if(resultTrue.end() - 1, resultTrue.end(), ::isspace), resultTrue.end());
		}

		// Same thing for false
		if(parts.size() > 2) {
			resultFalse = "";
			for(int i = 2; i < parts.size(); i++) {
				
				if(i > 2)
					resultFalse += " : ";
				resultFalse += parts[i];
			}
			resultFalse.erase(std::remove_if(resultFalse.begin(), resultFalse.begin() + 1, ::isspace), resultFalse.begin() + 1);
			resultFalse.erase(std::remove_if(resultFalse.end() - 1, resultFalse.end(), ::isspace), resultFalse.end());
		}

		// Anything that's left in the base expression that is NOT a logic expression needs to be hashed to an int
		// This is so we can actually compare strings. Cheeky little work around here :)
		//
		// Once we've done that, the final expression can be built
		std::vector<std::string> expTok = X::splitStringBySpace(expr);
		expr = "";
		for(int i = 0; i < expTok.size(); i++) {
			if(!X::isNumber(expTok[i])) {
				if(    expTok[i] != "="
					&& expTok[i] != "<"
					&& expTok[i] != ">"
					&& expTok[i] != "<="
					&& expTok[i] != ">="
					&& expTok[i] != "false"
					&& expTok[i] != "true") {

					expTok[i] = std::to_string(X::stringToNumber(expTok[i]));
				}
			}

			// Replace trues and falses with some maths that the function parser can understand
			if(expTok[i] == "true")
				expTok[i] = "(1=1)";
			if(expTok[i] == "false")
				expTok[i] = "(1=0)";

			if(i != 0)
				expr += " ";
			expr += expTok[i];
		}

		// At this point, the logic expression is ready to go, so we can just pass it over to the function parser like with the maths
		FunctionParser fparser;
		int res = fparser.Parse(expr, "x");

		if(res < 0) {

			double vals[] = { 0 };
			double result = fparser.Eval(vals);

			if(result == 0)
				output = resultFalse;
			if(result == 1)
				output = resultTrue;

			// Recurse until just a std::string
			return defaultPropertyLambda(output);
		}

		else
			std::cout << "Lambda: Error in expression! '" << output << "' at position: " << res << std::endl;

	}

	// Anything else is treated as a std::string literal, so is just simply returned

	return output;
}

Property::Property() {
	
}

// Constructor
Property::Property(std::string key, std::string value) {
	_key = key;
	updateValue(value);
}

std::string Property::getKey() {
	return _key;
}

// Returns the raw value
// TODO: change this to getRawValue()
std::string Property::getValue() {
	return _value;
}

// Any time the actual value is needed, getEvaluated() should be called
// TODO: change this to getValue()
std::string Property::getEvaluated() {
	return bindValue();
}

// Update the property with a new raw value.
// This will also update the lambda result
void Property::updateValue(std::string newVal) {
	_value = newVal;
	bindValue = std::bind(lambdaFunction, _value);
}

// Set and update the lambda
void Property::setLambda(std::function<std::string(std::string)> lambda) {
	lambdaFunction = lambda;
	bindValue = std::bind(lambdaFunction, _value);
}