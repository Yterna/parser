
// Parser API implementation
//
// Written by X.Hunt

#include <string>
#include <sstream>
#include <functional>
#include <iostream>
#include <regex>

#include "ParserAPI.h"
#include "CallbackManager.h"

ParserAPI ParserAPI::INSTANCE;


ParserAPI* ParserAPI::getInstance() {
	return &ParserAPI::INSTANCE;
}

// Database
void ParserAPI::setDatabaseName(std::string dbName) {
	_parserController.setDatabaseName(dbName);
}

std::string ParserAPI::getDatabaseName() {
	return _parserController.getDatabaseName();
}

void ParserAPI::createDatabase() {
	_parserController.createDB();
}

void ParserAPI::createAndNameDatabase(std::string dbName) {
	setDatabaseName(dbName);
	createDatabase();
}

// Parser
int ParserAPI::parseDirectory(std::string directory) {
	return _parserController.parseDirectory(directory);
}

int ParserAPI::parseFile(std::string file) {
	return _parserController.parse(file);
}

// Objects
DefinableObject ParserAPI::getObjectByName(std::string objectClassName) {
	return SQLWrapper::getObjectByName(getDatabaseName(), objectClassName);
}

std::vector<DefinableObject> ParserAPI::getAllObjects() {
	return SQLWrapper::getDefObjs(getDatabaseName());
}

void ParserAPI::storeObject(DefinableObject object) {
	SQLWrapper::storeDefObj(getDatabaseName(), &object);
}

void ParserAPI::displayObject(DefinableObject object) {
	DefinableObject::display(&object);
}

void ParserAPI::displayObject(std::string objectClassName) {
	displayObject(getObjectByName(objectClassName));
}

// Callback
void ParserAPI::registerCallback(std::string regexString, std::function<std::string(std::string)> callbackFunc) {
	CallbackManager::INSTANCE.registerCallback(regexString, callbackFunc);
}

std::string ParserAPI::runCallback(std::string token) {
	return CallbackManager::INSTANCE.runCallback(token);
}
