
// Callback Manager
//
// The callback manager manages all callbacks, and their insertion into the program from an external call
//
// Written by X.Hunt

#ifndef CALLBACK_MGER
#define CALLBACK_MGER

// Includes
#include <unordered_map>
#include <string>
#include <regex>
#include <functional>
#include <vector>
#include <iostream>

class CallbackManager {

private:
	// Internal map of all callback functions, keyed by their regex string
	std::unordered_map<std::string, std::function<std::string(std::string)>> _callbacks;

public:

	// Singleton instance
	static CallbackManager INSTANCE;

	// Registers a regex string to a callback
	void registerCallback(std::string regexString, std::function<std::string(std::string)> callbackFunc);

	// Pass a string to try and run a callback on. If it fails, the string is returned
	std::string runCallback(std::string token);

};

#endif