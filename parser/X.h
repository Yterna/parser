
#ifndef XLIB_H
#define XLIB_H

#include <vector>
#include <string>
#include <stack>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>

// This is a convenience library of helpful things
namespace X {

// String
void split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

int stringToNumber(const std::string &str);
bool isNumber(const std::string &str);

// Vector things
std::vector<std::string> splitStringBySpace(std::string line);

std::string replaceAll(std::string str, const std::string& from, const std::string& to);

// Stack
void printStack(std::stack<std::string>* stack);

}

#endif