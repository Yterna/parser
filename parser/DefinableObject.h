
// DefinableObject header
//
// A definable object is a collection of properties
//
// Written by X.Hunt

#ifndef DEFOBJ_H
#define DEFOBJ_H

// Includes
#include <unordered_map>
#include <string>

#include "Property.h"

// ===================

class DefinableObject {

public:
	DefinableObject();

	/* Static methods */
	static void copyProperties(std::unordered_map<std::string, Property>* newMap, DefinableObject* obj);
	static void display(DefinableObject* obj);

	/* Property methods */
	std::unordered_map<std::string, Property>* getProperties();
	Property getProperty(std::string key);
	void setProperty(std::string key, std::string value);
	bool hasProperty(std::string key);
	void removeProperty(std::string key);
	void clear();
	void populateParentProperties(std::string dbName);

	/* Everything else */
	void setClassName(std::string name);
	std::string getClassName();

private:
	// Internal mapping of property names to a property
	std::unordered_map<std::string, Property> _properties;
	std::string className;

};

#endif