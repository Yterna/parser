
// Parser controller

#include "ParserController.h"

#include <iostream>
#include <fstream>
#include <vector>

#include "Property.h"
#include "X.h"

// https://github.com/cxong/tinydir
#include "lib/tinydir.h"

void ParserController::setDatabaseName(std::string dbName) {
	databaseName = dbName;
}

std::string ParserController::getDatabaseName() {
	return databaseName;
}

int ParserController::createDB() {
	if(databaseName == "") {
		std::cerr << "Parser: Error: No database set!" << std::endl;
		return EXIT_FAILURE;
	}

	if(sqlWrapper.createDB(databaseName) == EXIT_SUCCESS) {
		dbReady = true;
		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}

// Parses a complete directory, including all subdirectories
int ParserController::parseDirectory(std::string dirName) {

	std::cout << "Parsing directory: " << dirName << std::endl;

	// tinydir is a great library
	tinydir_dir dir;
	tinydir_open(&dir, dirName.c_str());

	while (dir.has_next) {
		tinydir_file file;
		tinydir_readfile(&dir, &file);

		std::string fileName = file.name;
		std::string extension = file.extension;

		// Only look at files NOT prefixed with a dot (hidden files)
		std::string prefix = ".";
		if (fileName.compare(0, prefix.size(), prefix)) {
			std::cout << "Found: " << fileName;

			// If a directory is found, parse that now
			//
			// NOTE: This has the potential to stack overflow...
			if (file.is_dir) {
				std::cout << "/" << std::endl;
				parseDirectory(file.path);
			}

			// Found a file
			else {
				std::cout << std::endl;
				// Found a ydl file. Send it off to the parser!
				if(extension.compare("ydl") == 0) {
					parse(file.path);
				}

				// Dont parse what we dont need to!
				else {
					std::cout << "Skipping file!" << std::endl;
				}
			}
		}

		// Ignore hidden files (things begining with a dot)
		else {
			//std::cout << "Skipping file!" << std::endl;
		}

		tinydir_next(&dir);
	}

	tinydir_close(&dir);

	return EXIT_SUCCESS;
}

// Actually parses a file
int ParserController::parse(std::string file) {

	// Return an error if the DB is not set up
	if(!dbReady) {
		std::cerr << "Parser: Error: Database not ready. Start it with createDB()" << std::endl;
		return EXIT_FAILURE;
	}

	parser.setSingleFile(file);
	std::cout << "Parser: Using file: " << parser.getSingleFile() << std::endl;

	// Actually parse the file.
	// The objects generated are added to the database in the parser.parse()
	std::vector<DefinableObject> defObjs;
	if(parser.parse(&defObjs) == EXIT_SUCCESS) {
		return EXIT_SUCCESS;

	}

	return EXIT_FAILURE;

}


