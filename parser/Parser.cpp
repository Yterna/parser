
// Parser implementation
//
// Written by X.Hunt

#include "Parser.h"

#include <regex>
#include <iostream>
#include <fstream>

#include "X.h"
#include "Property.h"
#include "SQLWrapper.h"
#include "ParserAPI.h"

// =======================

int Parser::parse(std::vector<DefinableObject>* objects) {

	// Regex pattern strings
	std::regex REGEX_COMMENT("^[ \t\r\n\f]*#.*");
	std::regex REGEX_PROPERTY_KEY("^.*:");
	std::regex REGEX_PROPERTY_VALUE("^.*");
	std::regex REGEX_LIST_START("^\\[");
	std::regex REGEX_LIST_END("^\\]");
	std::regex REGEX_LINE_SEPERATOR("\n");
	std::regex REGEX_CLASS_HEADER("^<.*:");
	std::regex REGEX_CLASS_FOOTER("^>");

	int lineNumber = 1;

	if(getSingleFile() == "") {
		return EXIT_FAILURE;
	}

	std::ifstream defFile(getSingleFile());

	std::cout << "Reading file..." << std::endl;
	std::vector<std::string> tokens;
	std::vector<std::string> lines;

	// Tokenise the file. Each token is seperated by a space (or newline)
	std::string line;
	while(getline(defFile, line)) {

		lines.push_back(line);

		std::vector<std::string> lineTokens = X::splitStringBySpace(line);
		lineTokens.push_back("\n");

		for(auto &token : lineTokens) {
			tokens.push_back(token);
		}
	}

	// List of all defObjs created
	std::vector<DefinableObject*> defObjs;
	DefinableObject* defObj = new DefinableObject();

	// States
	bool inProperty = false;
	bool inList = false;

	std::string listName = "";

	// Start the actual parsing of the tokens
	int index = 0;
	while(index < tokens.size()) {

		std::string tok = tokens[index];

		// Comment, ignore
		if(regex_match(tok, REGEX_COMMENT)) {

			while(true) {
				std::string val = tokens[++index];

				if(regex_match(val, REGEX_LINE_SEPERATOR)) {
					inProperty = false;
					lineNumber++;
					break;
				}
			}
		}

		// Class header
		else if(regex_match(tok, REGEX_CLASS_HEADER)) {

			std::string header = tok.substr(1, tok.size() - 2);
			defObj->setClassName(header);
		}

		// Key:property value
		else if(regex_match(tok, REGEX_PROPERTY_KEY)) {

			// Remove the final ":" from the std::string
			std::string key = tok.substr(0, tok.size() - 1);

			if(inList)
				key = listName + "." + key;

			inProperty = true;
			std::string prop = "";

			while(inProperty) {
				std::string val = tokens[++index];

				if(regex_match(val, REGEX_PROPERTY_VALUE)) {

					if(regex_match(val, REGEX_LIST_START)) {
						inList = true;
						listName = key;
						prop += " LIST";
					}

					else {
						prop += " " + val;
					}
				}

				else if(regex_match(val, REGEX_LINE_SEPERATOR)) {
					inProperty = false;
					lineNumber++;
					break;
				}
			}

			// Remove any leading whitepsace
			size_t first = prop.find_first_not_of(' ');
			size_t last = prop.find_last_not_of(' ');
			prop = prop.substr(first, (last-first+1));

			defObj->setProperty(key, prop);

		}

		else if(regex_match(tok, REGEX_LIST_END)) {
			inList = false;
		}

		// Class footer
		else if(regex_match(tok, REGEX_CLASS_FOOTER)) {

			// Clean up any parent tags and __internals

			std::string finalName = defObj->getClassName();
			if(defObj->hasProperty("parent")) {
				finalName = defObj->getProperty("parent").getValue() + "." + defObj->getClassName();
				finalName = finalName.substr(1, finalName.size());
			}
			defObj->setClassName(finalName);
			defObj->setProperty("__internalClassName", finalName);
			// Internal object ID is not guaranteed to be identical between runtimes (but should be)
			defObj->setProperty("__internalID", std::to_string(ID));
			ID++;

			defObjs.push_back(defObj);
			defObj = new DefinableObject();
		}

		// Ignore any newlines
		else if(regex_match(tok, REGEX_LINE_SEPERATOR)) {
			//std::cout << "Found LINE SEPERATOR. Skipping!" << std::endl;
			lineNumber++;
		}

		else {
			std::cout << "Syntax error? Line " << lineNumber << ": " << lines[lineNumber - 1] << std::endl;
		}

		index++;

	}

	// Once all the things are done, update any parent properties and then store everything back in the DB
	for(int i = 0; i < defObjs.size(); i++) {

		defObjs[i]->populateParentProperties(ParserAPI::getInstance()->getDatabaseName());

		objects->push_back(*defObjs[i]);
		SQLWrapper::storeDefObj(ParserAPI::getInstance()->getDatabaseName(), defObjs[i]);

	}

	// Clean up and exit
	delete defObj;
	
	std::cout << "Done!" << std::endl;
	return EXIT_SUCCESS;

}

void Parser::setSingleFile(std::string file) {
	Parser::singleFile = file;
}

std::string Parser::getSingleFile() {
	return Parser::singleFile;
}

void Parser::setRootDirectory(std::string dir) {
	Parser::rootDirectory = dir;
}

std::string Parser::getRootDirectory() {
	return Parser::rootDirectory;
}