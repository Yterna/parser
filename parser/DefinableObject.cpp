
// DefinableObject class implementation
//
// Written by X.Hunt

#include "DefinableObject.h"
#include "X.h"
#include "SQLWrapper.h"

#include <vector>

DefinableObject::DefinableObject() {
	
}

// Copies properties from one DefObj to a new mapping
void DefinableObject::copyProperties(std::unordered_map<std::string, Property>* newMap, DefinableObject* obj) {

	std::unordered_map<std::string, Property>* properties = obj->getProperties();

	for (auto p = properties->begin(); p != properties->end(); ++p) {
		newMap->emplace(p->first, p->second);
	}
}

// Displays a defObj
void DefinableObject::display(DefinableObject* obj) {
	std::cout << "=================" << std::endl;
	std::cout << "DefObj (" << obj->getClassName() <<  ") contains:" << std::endl;

	for(auto& x: *obj->getProperties()) {
		std::cout << x.first << " => " << x.second.getEvaluated() << std::endl;
	}

	std::cout << "=================" << std::endl;
}

// Returns a pointer to the full value mapping
std::unordered_map<std::string, Property>* DefinableObject::getProperties() {
	return &_properties;
}

// Returns a property
Property DefinableObject::getProperty(std::string key) {
	return _properties[key];
}

// Sets the value of a key
// If the key exists, it is overwritten
void DefinableObject::setProperty(std::string key, std::string value) {
	Property prop(key, value);
	_properties[key] = prop;
}

// Removes a key and its value
void DefinableObject::removeProperty(std::string key) {
	_properties.erase(key);
}

// Checks if a property exists
bool DefinableObject::hasProperty(std::string key) {
	return _properties.find(key) != _properties.end();
}

// Clears all properties and the defObj's classname
void DefinableObject::clear() {
	className.clear();
	_properties.clear();
}

// Sets the classname and the __internalClassName
void DefinableObject::setClassName(std::string name) {
	className = name;
	setProperty("__internalClassName", className);
}

// Returns the className
std::string DefinableObject::getClassName() {
	return className;
}

// Populates this defObj with the properties of its parent
void DefinableObject::populateParentProperties(std::string dbName) {

	std::unordered_map<std::string, Property>* myProperties = new std::unordered_map<std::string, Property>();
	copyProperties(myProperties, this);

	// No parent, let's get out of here
	if(!hasProperty("parent")) {
		return;
	}

	else {

		std::string parent = getProperty("parent").getValue();
		parent.erase(std::remove(parent.begin(), parent.end(), '@'), parent.end());		// Remove any @'s in the parent name (just want the raw text)
		DefinableObject parentObj = SQLWrapper::getObjectByName(dbName, parent);

		std::unordered_map<std::string, Property>* properties = parentObj.getProperties();

		// Get the parent properties and add them to this
		for (auto p = properties->begin(); p != properties->end(); ++p) {
			Property property = p->second;
			setProperty(property.getKey(), property.getValue());
		}

		// Fix up the actual properties
		for (auto p = myProperties->begin(); p != myProperties->end(); ++p) {
			Property property = p->second;
			setProperty(property.getKey(), property.getValue());
		}
	}
}




