
#include <vector>
#include <string>
#include <functional>
#include <iostream>

using namespace std;

int main(int argc, char const *argv[]) {
	
	auto l1 = [&](std::string s) {
		return stoi(s);
	};
	
	auto l2 = [&](std::string s) { 
		std::cout << s << std::endl; 
		return 0;
	};

	vector<std::function<int(std::string)>> v;
	v.push_back(l1);
	v.push_back(l2);

	std::cout << v[0]("30") << std::endl;
	v[1]("test");

	vector<std::function<int()>> v2;

	v2.push_back(std::bind(l1, "30"));
	std::cout << v2[0]() << std::endl;

	v2.push_back(std::bind(l2, "test"));
	std::cout << v2[1]() << std::endl;

}
