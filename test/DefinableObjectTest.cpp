
// Tests the definableObject class
//
// Written by X.Hunt

#include <string>
#include <unordered_map>
#include <iostream>

#include "DefinableObject.cpp"

using namespace std;

// Display what's in the DO
void dispDefObj(DefinableObject* defObj) {
	for(auto& x: *defObj->getProperties()) {
		std::cout << x.first << ":" << x.second << std::endl;
	}
}

int main(int argc, char const *argv[]) {
	
	std::cout << "==========================" << std::endl;
	std::cout << " Testing DefinableObject! " << std::endl;
	std::cout << "==========================" << std::endl;

	// Create a new DO
	DefinableObject defObj;

	// Give it some values
	defObj.setValue("Key1", "value1");
	defObj.setValue("Key2", "value2");
	defObj.setValue("Key3", "value3");

	std::cout << "Created a new DefinableObject with these key:value pairs" << std::endl;
	dispDefObj(&defObj);
	
	// Update a value
	defObj.setValue("Key2", "value4");

	std::cout << "Updated a value in the DefinableObject" << std::endl;
	dispDefObj(&defObj);
	

	std::cout << "Removing a value" << std::endl;
	// Remove a value
	defObj.removeValue("Key3");
	dispDefObj(&defObj);

	std::cout << "========================" << std::endl;
	std::cout << "        Complete!       " << std::endl;
	std::cout << "========================" << std::endl;

}