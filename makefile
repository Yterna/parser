# Compiler to use. Since this is on mac, it's clang
CC = g++
C_ARGS = -Wall -std=c++14 -l sqlite3 -o $(OUTDIR)/
C_ARGS_LIB = -Wall -std=c++14 -l sqlite3 -c

TOOL_DIR = ./mingw32/win32-gcc/bin/i586-mingw32-gcc

# Files to use

SOURCES = $(wildcard parser/*.cpp) $(wildcard parser/lib/*.cpp)
MAIN = $(SOURCES) main.cpp
O_FILES := $(wildcard *.o)

# Directory to put the files
OUTDIR = bin

# Make all files
all: main

main: $(OUTDIR)
	$(CC) $(C_ARGS)main $(MAIN)
	./$(OUTDIR)/main

oFiles: $(OUTDIR)
	cd $(OUTDIR)
	$(CC) $(C_ARGS_LIB) $(MAIN)

lib: $(OUTDIR)
	$(TOOL_DIR) -l sqlite3 -c $(SOURCES)
	$(TOOL_DIR) -shared -o parser.dll $(O_FILES) -Wl,--out-implib,libparser.a

# Makes just main
# main: $(OUTDIR)
# 	$(CC) $(C_ARGS)main main.cpp
# 	./$(OUTDIR)/main
	
# Makes just defObj
defObj: $(OUTDIR)
	$(CC) $(C_ARGS)DefinableObjectTest DefinableObjectTest.cpp

# Tests defObj
defObjTest: defObj
	./$(OUTDIR)/DefinableObjectTest

lambda: $(OUTDIR)
	$(CC) $(C_ARGS)lambda lambdaTest.cpp
	./$(OUTDIR)/lambda

sql: $(OUTDIR)
	$(CC) $(C_ARGS)sqlTest test/sqlTest.cpp
	./$(OUTDIR)/sqlTest

# Makes the bin directory
$(OUTDIR):
	mkdir -p $(OUTDIR)

# Clean
clean:
	#rm -f $(OUTDIR)/*
	#rm -df $(OUTDIR)
	rm *.o