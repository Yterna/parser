# README #

# Yterna Definition Language Parser #

Parser for [Yterna Definition Language](http://engine.yterna.com/#!/documentation) files.

# Overview #

The parser is used to convert the definition files into usable C++ objects. These objects contain the properties defined in the definitions.
Parsed objects are stored in a small database. The parser will also:

 - Compute references between objects (`otherValue: @OtherClass.value`)
 - Evaluate properties of objects (such as `value: 1 + 2`)
 - Evaluate boolean expressions in properties (such as `value: {myValue = @OtherClass.value}`)
 - Allow external callbacks to be given to an object, to let it return something else, for example, a dice roll.

The API is used to bridge the internal workings of the parser, and an external application.

# API #

To use the API, `#import "ParserAPI.h"` in your code.

## Data Types ##

### Definable Object ###

`#import "DefinableObject.h"`

A collection of properties.

### Property ###

`#import "Property.h"`

A Key:Value mapping. The property also has a callback attached, which is used to convert the raw value into a meaningful string. Most of the time, you wont need to attach your own callback.

## Functions ##

 - **getInstance()**: Returns a pointer to the api INSTANCE object.

### Database Things ###

 - **setDatabaseName(string dbName)**: Sets the database name.
 - **getDatabaseName()**: Returns the name of the database.
 - **createDatabase()**: Creates a database with the path given in `setDatabaseName`.
 - **createAndNameDatabase(string dbName)**: Convenience function for naming and creating the database.

### Parser Things ###

 - **parseDirectory(string directory)**: Recursively parses all files and subfolders in the directory specified. This can be a relative directory from the application.
 - **parseFile(string filePath)**: Parses a single file, from the filePath specified. This can be a relative filepath from the application.

### Object Things ###

 - **getObjectByName(string objectClassName)**: Returns the DefinableObject with the specified className.
 - **displayObject(DefinableObject object)** OR **displayObject(string objectClassName)**: Prints the object specified to std::cout
 - **storeObject(DefinableObject object)**: Stores an object back into the database.

## Example ##

### definitions.ydl ###

```
# Test class base. This holds some values for TestClassActual
<TestClassBase:
    name: TestClassBase
    foo: bar
    numbers: 1 + 5
    aList: [
        listValue1: 1
        listValue2: 2
    ]
>

# Test class actual
<TestClassActual:
    parent: @TestClassBase
    name: TestClassActual
    result: @TestClassBase.numbers * 3
>
```

### main.cpp ###

```

#include "ParserAPI.h"

int main(int argc, char const *argv[]) {
    
    // api object. Use this to interact with the parser
    ParserAPI *api = ParserAPI::getInstance();

    // Give the database a name and create it
    api->createAndNameDatabase("database.db");

    // Parse a file called "definitions.ydl". Parsing a directory works the same as this
    if(api->parseFile("definitions.ydl") == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    // Assuming all went well, get and display the object
    DefinableObject object = api->getObjectByName("TestClassBase.TestClassActual");
    api->displayObject(object); // Could also display as api->displayObject("TestClassBase.TestClassActual");

   return EXIT_SUCCESS;
}

```

### Output ###

```

DefObj (TestClassBase.TestClassActual) contains:
__internalID => 1
__internalClassName => TestClassBase.TestClassActual
name => TestClassActual
parent => @TestClassBase
foo => bar
numbers => 6.0
aList => LIST
aList.listValue1 => 1
aList.listValue2 => 2
result => 18.0

```