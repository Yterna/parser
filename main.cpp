
// Parser test
//
// This file tests the parser, and is NOT part of the parser module
//
// Written by X.Hunt

#include <iostream>
#include <vector>
#include <string>
#include <random>

#include "parser/ParserAPI.h"
#include "parser/X.h"

// Dice callback
// This testing callback returns a dice roll
static std::string diceCallback(std::string s) {

	X::replaceAll(s, "D", "d");
	std::vector<std::string> tokens = X::split(s, 'd');

	int count = stoi(tokens[0]);
	int sides = stoi(tokens[1]);
	int result = 0;

	std::random_device r;
	std::default_random_engine e1(r());
	std::uniform_int_distribution<int> uniform_dist(1, sides);

	for(int i = 0; i < count; i++) {
		int d = uniform_dist(e1);
		result += d;
		//std::cout << "Dice " << i << " rolled " << d << " for a total of " << result << std::endl;
	}

	return std::to_string(result);

}

// Access callback.
// This is a second testing callback, which returns a string
static std::string accessCallback(std::string s) {
	return "grass";
}

int help() {

	std::cout << "==========================" << std::endl;
	std::cout << "         HELP PAGE        " << std::endl;
	std::cout << "==========================" << std::endl;
	std::cout << "       help: show this page" << std::endl;
	std::cout << "     update: redo the parse" << std::endl;
	std::cout << "show <name>: shows the class specified" << std::endl;
	std::cout << "       exit: quits" << std::endl;
	std::cout << "==========================" << std::endl;

	return EXIT_SUCCESS;
}

int doExit() {
	return EXIT_SUCCESS;
}

int parse(std::string className) {
	ParserAPI *api = ParserAPI::getInstance();
	if(api->parseDirectory("definitions") == EXIT_FAILURE) {
		return EXIT_FAILURE;
	}

	api->displayObject(className);
	return EXIT_SUCCESS;
}

int parse() {
	ParserAPI *api = ParserAPI::getInstance();
	if(api->parseDirectory("definitions") == EXIT_FAILURE) {
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

// Main
int main(int argc, char const *argv[]) {
	
	std::cout << "==========================" << std::endl;
	std::cout << "       Parser Test!       " << std::endl;
	std::cout << "==========================" << std::endl;

	ParserAPI *api = ParserAPI::getInstance();

	std::cout << "==========================" << std::endl;
	std::cout << "       Creating DB!       " << std::endl;
	std::cout << "==========================" << std::endl;

	api->createAndNameDatabase("bin/parser.db");

	api->registerCallback("^(\\d+)[dD](\\d+)$", diceCallback);
	api->registerCallback("^Access$", accessCallback);
	
	std::cout << "==========================" << std::endl;
	std::cout << "      Starting parse!     " << std::endl;
	std::cout << "==========================" << std::endl;

	help();

	std::string line;
	std::string command;
	std::cout << "> ";

	while(getline(std::cin, line)) {

			if(!line.empty()) {

			command = X::splitStringBySpace(line)[0];

			if(command.compare("help") == 0) {
				help();
			}

			if(command.compare("update") == 0) {
				if(parse() == EXIT_FAILURE) {
					return EXIT_FAILURE;
				}
			}

			if(command.compare("show") == 0) {
				if(X::splitStringBySpace(line).size() > 1) {
					std::string name = X::splitStringBySpace(line)[1];
					api->displayObject(name);
				}

				else {
					std::cout << "No classname specified" << std::endl;
				}
			}

			if(command.compare("exit") == 0) {
				break;
			}
		}

		// else {
		// 	std::cout << "Unknown command. Try 'help'" << std::endl;
		// }

		std::cout << "> ";

	}

	return doExit();

}




